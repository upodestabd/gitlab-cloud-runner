# Gitlab Cloud Runner

Repository for configuration of general purpose GitLab Cloud Runner.

## Provisioning of infrastructure

Terraform is used to provision the infrastructure. Code can be found in  `terraform` folder. Terraform state is managed by GitLab. `.gitlab-ci.yml` has CI jobs to configure the Terraform state, validate the code, plan/diff and deploy the code to the cloud provider.

There is one optional `terraform destroy` job to remove the infrastructure.

Most configuration parameters are exposed using Terraform variables in `terraform/variables.tf`.

## Configuration of Kubernetes Runner/Executor

`gitlab-runner` contains the helm values file (configuration parameters) for `gitlab/gitlab-runner` helm chart, which deploys the GitLab Kubernetes Executor to the cluster. `.gitlab-ci.yml` contains CI jobs to diff and apply the helm chart.

There is one optional `helm uninstall` job to remove gitlab-runner helm chart.

## How to setup the environment

 * Create a Digital Ocean API token and save it as `DIGITALOCEAN_TOKEN` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected)
 * Add the `RUNNER_REGISTRATION_TOKEN` to the desired project/group under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected)
 * Run pipeline and execute manual `deploy` job with does a `terraform apply` (helm diff job may fail here)
 * Download Kubernetes config from Digital Ocean web panel
 * Add the Kubernetes config `base64` encoded as `kube_config` under [Settings > CI/CD > Variables](https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/settings/ci_cd) (masked and protected) - TODO: Automate this step with Terraform attributes)
 * Run pipeline again and execute manual `helm-install` job with does a `helm install`
 * Check GitLab Admin area for new Runner
