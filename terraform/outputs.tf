output "kubeconfig" {
  value = module.do.cluster_kubeconfig
  sensitive = true
}

output "cache_bucket" {
  value = var.cache_bucket
}

output "bucket_region" {
  value = var.bucket_region
}

output "cluster_subnet" {
  value = module.do.cluster_subnet
  description = "The subnet range from which IPs are allocated to pod endpoints"
}

output "cluster_service_subnet" {
  value = module.do.cluster_service_subnet
  description = "The subnet range from which IPs are allocated to service endpoints"
}

output "cluster_domain" {
  value = var.cluster_domain
}

output "container_registry_hostname" {
  value       = var.container_registry_hostname
  description = "The host name of reggie's load balancer"
}

output "container_registry_fqdn" {
  value       = module.do.container_registry_fqdn
  description = "The FQDN of reggie's load balancer"
}

output "docker_hub_mirror_hostname" {
  value       = var.docker_hub_mirror_hostname
  description = "The host name of the docker hub mirror"
}

output "docker_hub_mirror_fqdn" {
  value       = module.do.docker_hub_mirror_fqdn
  description = "The FQDN of the docker hub mirror"
}

output "buildkitd_max_connections" {
  value = var.buildkitd_max_connections
  description = "Maximum number of concurrent connections to each buildkitd instance"
}
