module "do" {
  source = "./digitalocean"

  # We must propagate all variables explicitly to the child module
  access_id                   = var.access_id
  bucket_region               = var.bucket_region
  cache_bucket                = var.cache_bucket
  cluster_auto_upgrade        = var.cluster_auto_upgrade
  cluster_domain              = var.cluster_domain
  cluster_ha                  = var.cluster_ha
  cluster_region              = var.cluster_region
  cluster_version_prefix      = var.cluster_version_prefix
  container_registry_hostname = var.container_registry_hostname
  node_pool_max_nodes         = var.node_pool_max_nodes
  node_pool_min_nodes         = var.node_pool_min_nodes
  secret_key                  = var.secret_key
}

# The namespace where the runner, executors, and buildkitd reside
resource "kubernetes_namespace" "gitlab_runner" {
  metadata {
    annotations = {
      name = "gitlab-runner"
    }

    labels = {
      istio-injection = "enabled"
    }

    name = "gitlab-runner"
  }
}

# The namespace where the prometheus stack resides
resource "kubernetes_namespace" "monitoring" {
  metadata {
    annotations = {
      name = "monitoring"
    }

    name = "monitoring"
  }
}

# The namespace for the ingress-nginx controller
resource "kubernetes_namespace" "ingress" {
  metadata {
    annotations = {
      name = "ingress"
    }

    name = "ingress"
  }
}

# The namespace for the cert-manager controller
resource "kubernetes_namespace" "certs" {
  metadata {
    annotations = {
      name = "certs"
    }

    name = "certs"
  }
}

resource "helm_release" "cert-manager" {
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  name       = "cert-manager"
  namespace  = kubernetes_namespace.certs.metadata[0].name
  version    = "1.10.1"

  set {
    name  = "installCRDs"
    value = "true"
  }
}


# The namespace for the istio service mesh
resource "kubernetes_namespace" "istio_system" {
  metadata {
    annotations = {
      name = "istio-system"
    }

    name = "istio-system"
  }
}

resource "helm_release" "istio-base" {
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart      = "base"
  name       = "istio-base"
  namespace  = kubernetes_namespace.istio_system.metadata[0].name
  version    = "1.16.1"
}

resource "helm_release" "istiod" {
  repository = "https://istio-release.storage.googleapis.com/charts"
  chart      = "istiod"
  name       = "istiod"
  namespace  = kubernetes_namespace.istio_system.metadata[0].name
  version    = helm_release.istio-base.version
}
