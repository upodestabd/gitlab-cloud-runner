resource "digitalocean_project" "gitlab_cloud_runner" {
  name        = "gitlab-cloud-runner"
  description = "A project for our GitLab CI cloud runners (https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner)"
  is_default  = true
  resources   = [
    digitalocean_kubernetes_cluster.cloud_runner.urn,
    digitalocean_domain.cluster_domain.urn,
    digitalocean_spaces_bucket.runner_cache.urn,
  ]
}

data "digitalocean_kubernetes_versions" "versions" {
  version_prefix = var.cluster_version_prefix
}

resource "digitalocean_kubernetes_cluster" "cloud_runner" {
  name         = "cloud-runner"
  region       = var.cluster_region
  ha           = var.cluster_ha
  version      = data.digitalocean_kubernetes_versions.versions.latest_version
  auto_upgrade = var.cluster_auto_upgrade

  maintenance_policy {
    start_time  = "04:00"
    day         = "sunday"
  }

  # Default node pool for the cluster to run things like kube operators and
  # monitoring. Additional node pools for specific CI workloads are defined
  # below
  node_pool {
    name       = "cloud-runner-pool"
    # DO cannot change instance size for existing node pools:
    # See https://github.com/digitalocean/terraform-provider-digitalocean/issues/558
    # Must run terraform destroy before deploying when changing this value.
    size       = "g-2vcpu-8gb"
    auto_scale = true
    min_nodes  = 2
    max_nodes  = 4
  }
}

# Define an additional storage optimized node pool for disk-bound CI workloads
resource "digitalocean_kubernetes_node_pool" "storage_optimized_pool" {
  cluster_id = digitalocean_kubernetes_cluster.cloud_runner.id

  name = "storage-optimized"
  size = "so1_5-4vcpu-32gb"
  auto_scale = true
  min_nodes  = var.node_pool_min_nodes
  max_nodes  = var.node_pool_max_nodes

  labels = {
    node-optimized-for = "disk-io"
  }

  # Add a NoExecute taint to repel all Pods that don't explicitly tolerate it.
  # This will keep kube-system and monitoring workloads off this pool
  taint {
    key = "gitlab.wikimedia.org/workload"
    value = "disk-io"
    effect = "NoExecute"
  }
}

# Define an additional cpu optimized node pool for cpu-bound CI workloads
resource "digitalocean_kubernetes_node_pool" "cpu_optimized_pool" {
  cluster_id = digitalocean_kubernetes_cluster.cloud_runner.id

  name = "cpu-optimized"
  size = "c-4"
  auto_scale = true
  min_nodes  = var.node_pool_min_nodes
  max_nodes  = var.node_pool_max_nodes

  labels = {
    node-optimized-for = "cpu"
  }

  # Add a NoExecute taint to repel all Pods that don't explicitly tolerate it.
  # This will keep kube-system and monitoring workloads off this pool
  taint {
    key = "gitlab.wikimedia.org/workload"
    value = "cpu"
    effect = "NoExecute"
  }
}

# DNS domain
resource "digitalocean_domain" "cluster_domain" {
  name = var.cluster_domain
}

# CNAME entry for the registry that points to the zone apex A record that the
# load balancer creates
resource "digitalocean_record" "container_registry" {
  domain = digitalocean_domain.cluster_domain.id
  type = "CNAME"
  name = var.container_registry_hostname
  value = "${var.cluster_domain}."
}

# Likewise for docker-hub-mirror
resource "digitalocean_record" "docker_hub_mirror" {
  domain = digitalocean_domain.cluster_domain.id
  type = "CNAME"
  name = var.docker_hub_mirror_hostname
  value = "${var.cluster_domain}."
}

# Create a DO Spaces bucket for runner and buildkitd caches
resource "digitalocean_spaces_bucket" "runner_cache" {
  name = var.cache_bucket
  region = var.bucket_region
}
