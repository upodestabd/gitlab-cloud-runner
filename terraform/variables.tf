variable "access_id" {
  description = "The access key ID used for Spaces API operations."
}

variable "secret_key" {
  description = "The secret access key used for Spaces API operations."
  type        = string
  sensitive   = true
}

variable "cache_bucket" {
  description = "Bucket used for caches"
  type        = string
  default     = "org-wikimedia-releng-cloud-runner-cache"
}

variable "bucket_region" {
  description = "Region of the spaces storage bucket"
  type        = string
  default     = "nyc3"
}

variable "cluster_region" {
  description = "Region of Kubernetes cluster"
  type        = string
  default     = "nyc3"
}

variable "cluster_ha" {
  description = "Switch to enable high available control plane"
  type        = bool
  default     = false
}

variable "cluster_auto_upgrade" {
  description = "Switch to enable automatic patch release upgrades"
  type        = bool
  default     = true
}

variable "cluster_domain" {
  description = "Domain name for the cluster"
  type        = string
  default     = "cloud.releng.team"
}

variable "cluster_version_prefix" {
  description = "The minor version of Kubernetes to use. Note that auto upgrade may update to a newer patch version."
  type        = string
  default     = "1.24."
}

variable "container_registry_hostname" {
  description = "Host name of reggie's load balancer"
  type        = string
  default     = "registry"
}

variable "docker_hub_mirror_hostname" {
  description = "Host name of the docker hub mirror"
  type        = string
  default     = "docker-hub-mirror"
}

variable "node_pool_min_nodes" {
  description = "Minimum number of machine size in each Kubernetes node pool"
  type        = number
  default     = 4
}

variable "node_pool_max_nodes" {
  description = "Maximum number of machine size in each Kubernetes node pool"
  type        = number
  default     = 20
}

variable "buildkitd_max_connections" {
  description = "Maximum number of concurrent connections to each buildkitd instance"
  type        = number
  default     = 10
}
